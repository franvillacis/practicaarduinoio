#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>
#include "arduino-serial-lib.h"
/*
struct lista{
    int temperatura;
    int humedad;
    struct lista *siguiente;
};

lista *first = NULL;
lista *actual = NULL;
lista *nodo;
void captur(){
   sleep(5000);
    nodo = new lista;
    nodo->temperatura = t;
    nodo->humedad = h;
    nodo->siguiente = NULL; 
    if(first == NULL){
       first = nodo;
       actual = nodo;
        
    }else{
       actual->siguiente = nodo;
       actual = nodo;
    }
}*/

float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int baudrate = 9600;  // default
        char humedad[10];
	fd = serialport_init("/dev/ttyACM0", baudrate);
	serialport_flush(fd);
	char h;
	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
	
        const int buf_max = 15;
        int timeout = 5000;
        char buf[buf_max];
        char eolchar = '\n';
	
        serialport_write(fd,h);

        memset(humedad,0,sizeof(humedad));

        serialport_read_until(fd, humedad, eolchar, sizeof(humedad), timeout);
        printf("%s",humedad);
	close( fd );
	return 0;	
}





/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}

